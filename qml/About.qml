/*
 * Copyright (C) 2021  franz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Network Speed Test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQControlsAbout


Page {
  id: aboutPage
  property var date: new Date()

  header: PageHeader {
    id: aboutPageHeader
    title: i18n.tr("About")

    extension: Sections {
      id: aboutPageHeaderSections
      anchors { left: parent.left; bottom: parent.bottom }
      model: [i18n.tr("About"), i18n.tr("Credits")]
      StyleHints { selectedSectionColor: "#ec5b0d" }
    }
  }

  QQControlsAbout.SwipeView {
    id: swipeView
    anchors { top: aboutPageHeader.bottom; left: parent.left; right: parent.right; bottom: parent.bottom }
    currentIndex: aboutPageHeaderSections.selectedIndex

    Item {
      width: swipeView.width
      height: swipeView.height

      Flickable {
        id: flickableAbout
        anchors.fill: parent
        contentHeight: dataColumn.height + units.gu(10) + dataColumn.anchors.topMargin

        Column {
          id: dataColumn
          spacing: units.gu(3)
          anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: units.gu(5) }

          UbuntuShape {
            width: Math.min(parent.width/1.7, parent.height/1.7)
            height: width
            anchors.horizontalCenter: parent.horizontalCenter
            radius: "medium"
            AnimatedImage {
              id: nstInfo
              width: parent.width - 23
              height: parent.height - 23
              anchors.centerIn: parent
              source: "../assets/networkSpeedTestInfo.gif"
            }
          }

          Column {
            width: parent.width
            Label {
              width: parent.width
              fontSize: "x-large"
              font.weight: Font.DemiBold
              horizontalAlignment: Text.AlignHCenter
              text: "Network Speed Test"
            }
            Label {
              width: parent.width
              horizontalAlignment: Text.AlignHCenter
              text: i18n.tr("Version %1").arg(root.appVersion)
            }
          }

          Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(2) }
            Label {
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              text: "Copyleft (C) 2021 - anyone"
            }
            Label {
              fontSize: "small"
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              linkColor: UbuntuColors.blue
              text: i18n.tr("Released under the terms of %1").arg("<a href=\"https://www.gnu.org/licenses/gpl-3.0.html\">the GNU GPL v3</a>")
              onLinkActivated: Qt.openUrlExternally(link)
            }
          }

          Label {
            width: parent.width
            wrapMode: Text.WordWrap
            fontSize: "small"
            horizontalAlignment: Text.AlignHCenter
            linkColor: UbuntuColors.blue
            text: i18n.tr("Source code available on %1").arg("<a href=\"https://gitlab.com/francesco.padovani81/ut-network-speed-test\">GitLab</a>")
            onLinkActivated: Qt.openUrlExternally(link)
          }
        }
      }
    }

    Item {
      width: swipeView.width
      height: swipeView.height

      Flickable {
        id: flickableCredits
        anchors.fill: parent
        contentHeight: dataColumnCredits.height + units.gu(10) + dataColumnCredits.anchors.topMargin

        Column {
          id: dataColumnCredits
          spacing: units.gu(3)
          anchors { top: parent.top; left: parent.left; right: parent.right; topMargin: units.gu(5) }

          Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(2) }
            Label {
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              fontSize: "large"
              font.weight: Font.DemiBold
              //text: "\n-------\n"
            }
            Label {
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              //fontSize: "large"
              text: "A HUGE thank to my 11 years old daughter, <b>Alice</b>: the cat animation it's all her own thing, from the idea to the 2D graphic realization!"
              
            }
          }
 
          Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(2) }
            Label {
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              fontSize: "large"
              font.weight: Font.DemiBold
              text: "\n-------\n"
            }
            Label {
              width: parent.width
              wrapMode: Text.WordWrap
              horizontalAlignment: Text.AlignHCenter
              //text: "Another special thank to to Matt Martz (sivel): this app uses source code from speedtest-cli project on <a href=\"https://github.com/sivel/speedtest-cli\">GitHub</a>, Copyright 2012 Matt Martz, licensed under the Apache License, Version 2.0. You may obtain a copy of the License at <a href=\"http://www.apache.org/licenses/LICENSE-2.0\">www.apache.org</a>"
              text: i18n.tr("Another special thank to Matt Martz (sivel): this app uses source code from speedtest-cli project on %1, Copyright 2012 Matt Martz, licensed under the Apache License, Version 2.0. You may obtain a copy of the License at %2").arg("<a href=\"https://github.com/sivel/speedtest-cli\">GitHub</a>").arg("<a href=\"http://www.apache.org/licenses/LICENSE-2.0\">www.apache.org</a>")
              onLinkActivated: Qt.openUrlExternally(link)
            }
          }
        }
      }
    }
    
    onCurrentIndexChanged: {
      aboutPageHeaderSections.selectedIndex=currentIndex
    }

  }
}
