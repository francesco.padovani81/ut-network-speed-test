/*
 * Copyright (C) 2021  franz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Network Speed Test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
//import Ubuntu.Components.ListItems 0.1
//import QtQuick.Controls 2.12
//import QtQuick.Layouts 1.3
//import Qt.labs.settings 1.0

//import "./"

Item {
  id: creditsPage

  ListModel {
    id: creditsModel
    Component.onCompleted: {
      creditsModel.append({ name: "Igor Sobociński - for substantive support", title: i18n.tr("Huge thanks to:")})
      creditsModel.append({ name: "Piotr Perzyński - for substantive support", title: i18n.tr("Huge thanks to:"), url: "https://github.com/stratek123"})
      creditsModel.append({ name: "Pavel Prosto - for example-calculator", title: i18n.tr("Huge thanks to:"), url: "https://github.com/pavelprosto94" })
      creditsModel.append({ name: "Johan Guerreros - for Vulgry", title: i18n.tr("Huge thanks to:"), url: "https://launchpad.net/~johangm90" })
      creditsModel.append({ name: "The UBports Foundation - for Ubuntu Touch", title: i18n.tr("Huge thanks to:"), url: "https://ubports.com/foundation/ubports-foundation" })
    }
  }
}
