/*
 * Copyright (C) 2021  franz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Network Speed Test is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 0.1
import QtQuick.Controls 2.12 as QQControls
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

import "./"


MainView {
  id: root
  objectName: 'mainView'
  applicationName: 'networkspeedtest.francesco'
  automaticOrientation: true
  width: units.gu(45)
  height: units.gu(75)

  property string appVersion: "1.1.0"

  Component {
    id: aboutPage
    About {}
  }


  PageStack{
    id: pageStack
    Component.onCompleted: push(rootPage)

    Page {
      id: rootPage
      anchors.fill: parent

      header: PageHeader {
        id: header
        title: i18n.tr('Network Speed Test')
        trailingActionBar.actions: [
          Action {
	    text: i18n.tr("About")
	    iconName: "info"
	    onTriggered: pageStack.push(aboutPage)
          }
        ]
      }

      Image {
        id: pageBackgroundImage
        source: "appPabboxFirstImg.png"
        cache: true
        smooth: false
        asynchronous: true
        fillMode: Image.Stretch
        anchors { bottom: parent.bottom; bottomMargin: parent.height/6; horizontalCenter: parent.horizontalCenter }
        height: (rootPage.height/2)
        width: (rootPage.width)
      }

      QQControls.Button {
        id: startTestButton
        anchors { top: parent.top; topMargin: parent.height/10; horizontalCenter: parent.horizontalCenter }
        text: i18n.tr("Start!")
        height: (rootPage.height/12)
        width: height * 3

        property string speedResults
        property var resultsList
 
        contentItem: Text {
          text: startTestButton.text
          font.pointSize: 100
          minimumPointSize: 10
          fontSizeMode: Text.Fit
          color: startTestButton.down ? "#ffffff" : "#000000"
          horizontalAlignment: Text.AlignHCenter
          verticalAlignment: Text.AlignVCenter
          elide: Text.ElideRight
          wrapMode: Text.WordWrap
          padding: (parent.width/20)
        }

        background: Rectangle {
          color: startTestButton.down ? "#d26306" : "#ffb233"
          opacity: 0.5
          border.color: startTestButton.down ? "#d26306" : "#d26306"
          border.width: 1
          radius: 20
        }

        onClicked: {
          startTestButton.text="test running...";
          speedResultsRect.visible=false;
          speedRunning.visible=true;
          speedRunning.playing=true;
          python.call('testuggine.main', ['start'], function(returnValue) {
            resultsTextRight.speedResults=returnValue;
            resultsTextRight.resultsList=resultsTextRight.speedResults.split(';');
            resultsTextRight.text=resultsTextRight.resultsList[0]+"\n"+resultsTextRight.resultsList[1]+"\n"+resultsTextRight.resultsList[2];
            console.log('testuggine.main returned ' + returnValue);
            speedRunning.playing=false;
            speedRunning.visible=false;
            speedResultsRect.visible=true;
            startTestButton.text="Start again!";
          })
        }
      }

      Rectangle {
        id: speedResultsRect
        visible: false
        anchors { top: startTestButton.bottom; topMargin: parent.height/10; horizontalCenter: parent.horizontalCenter }
        width: (rootPage.width/5*4)
        height: (width/4*3)
        color: "#f7dc6f"
        opacity: 0.8

        Text {
          id: resultsTextLeft
          anchors.left: parent.left
          width: (parent.width/2)
          height: parent.height
          leftPadding: (parent.width/20)
          text: "Download:\nUpload:\nPing:"
          font.pointSize: (parent.height/15)
          font.bold: true
          lineHeight: 2.5
          color: "#000000"
          horizontalAlignment: Text.AlignLeft
          verticalAlignment: Text.AlignVCenter
          elide: Text.ElideRight
        }

        Text {
          id: resultsTextRight
          anchors.right: parent.right
          width: (parent.width/2)
          height: parent.height
          rightPadding: (parent.width/20)
          text: "?\n??\n???"
          font.pointSize: (parent.height/15)
          font.bold: true
          font.italic: true
          lineHeight: 2.5
          color: "#000000"
          horizontalAlignment: Text.AlignRight
          verticalAlignment: Text.AlignVCenter
          elide: Text.ElideRight

          property string speedResults
          property var resultsList
        }
      }

      Rectangle {
        id: speedStatusRect
        visible: true
        anchors { bottom: parent.bottom; bottomMargin: parent.height/6; horizontalCenter: parent.horizontalCenter }
        height: (rootPage.height/2)
        width: (rootPage.width)
        color: "transparent"

        AnimatedImage {
          id: speedRunning
          source: "appPabboxAnimation.gif"
          playing: false
          cache: false
          fillMode: Image.Stretch
          anchors.fill: parent
          visible: false
        }
      }

    }
  }

  Python {
    id: python

    Component.onCompleted: {
      addImportPath(Qt.resolvedUrl('../src/'));
      //importModule_sync("example")
      importModule('testuggine', function() {
        console.log('module imported');
      });
    }

    onError: {
      console.log('python error: ' + traceback);
    }
  }

}
