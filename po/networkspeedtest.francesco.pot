# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the networkspeedtest.francesco package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: networkspeedtest.francesco\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-02 17:08+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/About.qml:28 ../qml/About.qml:33 ../qml/Main.qml:57
msgid "About"
msgstr ""

#: ../qml/About.qml:33
msgid "Credits"
msgstr ""

#: ../qml/About.qml:83
msgid "Version %1"
msgstr ""

#: ../qml/About.qml:101
msgid "Released under the terms of %1"
msgstr ""

#: ../qml/About.qml:112
msgid "Source code available on %1"
msgstr ""

#: ../qml/About.qml:168
msgid ""
"Another special thank to Matt Martz (sivel): this app uses source code from "
"speedtest-cli project on %1, Copyright 2012 Matt Martz, licensed under the "
"Apache License, Version 2.0. You may obtain a copy of the License at %2"
msgstr ""

#: ../qml/Credits.qml:32 ../qml/Credits.qml:33 ../qml/Credits.qml:34
#: ../qml/Credits.qml:35 ../qml/Credits.qml:36
msgid "Huge thanks to:"
msgstr ""

#: ../qml/Main.qml:54 networkSpeedTest.desktop.in.h:1
msgid "Network Speed Test"
msgstr ""

#: ../qml/Main.qml:79
msgid "Start!"
msgstr ""
