# UT Network Speed Test

A very basilar Ubuntu Touch app to test your network speed (download, upload and ping).

## Installation

Search "network speed test" in the OpenStore and install the app.

<a href="https://open-store.io/app/networkspeedtest.francesco"><img src="https://open-store.io/badges/en_US.png" alt="OpenStore" /></a>

## Usage
Simply run the app and tap on "Start!" button to start the network speed test.

## Contributing
This is my very first try in creating an application (generally speaking... not only for UT). And since I'm pretty much like a goat with a keyboard, I'm open to contributions for anything. Thanks in advance.

## Credits and acknowledgment
Thanks to Matt Martz (sivel):
this project uses source code from the file speedtest.py (here renamed testuggine.py) from https://github.com/sivel/speedtest-cli, Copyright 2012 Matt Martz, licensed under the Apache License, Version 2.0. You may obtain a copy of the License at<br>
<http://www.apache.org/licenses/LICENSE-2.0><br>
Modifications have been notified into the header of the file itself, and consist of:
- Addition of a 'test' function to get network speed data.
- Change of the 'main' function to use the 'test' one and fit App needs.

## License
Copyright (C) 2021  francesco padovani

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

